'''
Created on 3 avr. 2018

@author: Kwasniewski

This program is a wizard automating the Linkam acceptance tests at Xenocs. It assumes that the Linkam stage is
connected directly to the test PC, which runs this program.
'''

import wx
#import pylinkam.icons as icons
#import pylinkam.linkam as linkam
from wx.lib.pubsub import pub
import icons
import linkam
import report
from time import strftime
import csv
import os
import logging
import threading
import shutil
from time import time
import platform
import tempfile
import pathlib
import argparse

try:
    import win32security
    import win32api
    import pywintypes
except ImportError:
    import ldap3

# create logger
logger = logging.getLogger('linkam_test')
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create a file handler
global linkam_logfile
tmp_dir = tempfile.gettempdir()
linkam_logfile = '%s%slinkam_%s.log' % (tmp_dir,os.path.sep,strftime('%Y-%m-%d-%H%M%S'))
fh = logging.FileHandler(linkam_logfile)
fh.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch and fh
ch.setFormatter(formatter)
fh.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

########################################################################

def roundup(x):
    return x if x % 100 == 0 else x + 100 - x % 100


class WxTextCtrlHandler(logging.Handler):
    def __init__(self, ctrl):
        logging.Handler.__init__(self)
        self.ctrl = ctrl

    def emit(self, record):
        s = self.format(record) + '\n'
        wx.CallAfter(self.ctrl.WriteText, s)
        #wx.Yield()

class DataContainer(object):
    '''Data container, storing the data and plotting
    '''
    def __init__(self, outFile):
        logger.debug('DataContainer constructor')
        self.data = {}
        self.timestamp = [0]
        self.outFile = outFile
        self.tmpDir = tempfile.gettempdir()
        self.tmpFile = self.tmpDir + os.path.sep + self.outFile
        self.logFile = ''
        #logger.info('Data will be temporarily saved to %s' % self.tmpFile)

    def updateData(self, data):
        for key in data.keys():
            if key not in self.data.keys():
                self.data[key] = [data[key]]
            else:
                self.data[key].append(data[key])
        self.appendToFile(data)
        wx.Yield()

    def appendToFile(self, data):
        if not os.path.isfile(self.tmpFile):
            #logger.debug('File %s does not exist, creating a new one.' % self.outFile)
            with open(self.tmpFile, 'w') as f:
                writer = csv.writer(f)
                writer.writerow(self.data.keys())
        else:
            #logger.debug('File %s already there - appending' % self.outFile)
            with open(self.tmpFile, 'a') as f:
                fieldnames = data.keys()
                writer = csv.DictWriter(f, fieldnames=fieldnames)
                writer.writerow(data)

    def moveToFinalDir(self):
        logger.debug('Moving %s to %s' % (self.tmpFile, self.outFile))
        self.logFile = self.outFile[:-4] + '.log'
        logger.debug('Moving %s to %s' % (linkam_logfile, self.logFile))
        try:
            shutil.move(self.tmpFile, self.outFile)
            shutil.copy(linkam_logfile, self.logFile)
        except:
            # Give it another chance
            shutil.move(self.tmpFile, self.outFile)
            shutil.copy(linkam_logfile, self.logFile)


class WizardPage(wx.Panel):
    """A Simple wizard page"""

    def __init__(self, parent, title):
        """Constructor"""
        logger.debug('WizardPage constructor')
        wx.Panel.__init__(self, parent)

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer = sizer
        self.SetSizer(sizer)

        #title = wx.StaticText(self, -1, title)
        #title.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))
        #sizer.Add(title, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        #sizer.Add(wx.StaticLine(self, -1), 0, wx.EXPAND|wx.ALL, 5)
        self.initializeUI(title)

    def initializeUI(self, title):
        # create grid layout manager
        self.sizer = wx.GridBagSizer()
        self.SetSizer(self.sizer)
        titleLabel = wx.StaticText(self, -1, title)
        titleLabel.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.addWidget(titleLabel, (0, 1), (1,6))

    def addWidget(self, widget, pos, span):
        self.sizer.Add(widget, pos, span, wx.EXPAND)


class WizardPanel(wx.Panel):
    """"""

    def __init__(self, parent, **kwargs):
        """Constructor"""
        logger.debug('WizardPanel constructor')
        wx.Panel.__init__(self, parent=parent)
        self.pages = []
        self.page_num = 0
        self.logFile = 'none'
        self.user_name = 'none'
        self.authenticated = False

        # Check for test operation mode
        if 'opmode' in kwargs.keys():
            self.opmode = kwargs['opmode']
        else:
            self.opmode = 'normal'
        logger.debug('opmode = %s' % self.opmode)

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.panelSizer = wx.BoxSizer(wx.VERTICAL)
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)

        # add abort/prev/next buttons
        self.abortBtn = wx.Button(self, label="Abort")
        self.abortBtn.Bind(wx.EVT_BUTTON, self.onAbort)
        btnSizer.Add(self.abortBtn, 0, wx.ALL|wx.ALIGN_RIGHT, 5)
        self.abortBtn.Hide()
        self.validateBtn = wx.Button(self, label="Validate")
        self.validateBtn.Bind(wx.EVT_BUTTON, self.validateStageInfo)
        btnSizer.Add(self.validateBtn, 0, wx.ALL|wx.ALIGN_RIGHT, 5)
        self.validateBtn.Hide()
        self.prevBtn = wx.Button(self, label="Previous")
        self.prevBtn.Bind(wx.EVT_BUTTON, self.onPrev)
        btnSizer.Add(self.prevBtn, 0, wx.ALL|wx.ALIGN_RIGHT, 5)
        self.prevBtn.Disable()
        self.nextBtn = wx.Button(self, label="Next")
        self.nextBtn.Bind(wx.EVT_BUTTON, self.onNext)
        btnSizer.Add(self.nextBtn, 0, wx.ALL|wx.ALIGN_RIGHT, 5)

        # finish layout
        self.mainSizer.Add(self.panelSizer, 1, wx.EXPAND)
        self.mainSizer.Add(btnSizer, 0, wx.ALIGN_RIGHT)
        self.SetSizer(self.mainSizer)

        # Timer
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.onUpdate, self.timer)
        self.timer.Start(1000)

        # Connection status
        self.connectionStatus = 0

        self.dataContainer = DataContainer('test_res_%s.csv' % strftime('%Y-%m-%d-%H%M%S'))
        self.paused = False

        # Stop test event
        self.stop_test_event = threading.Event()

        # Stage information
        self.stage_name = ''
        self.stage_sn = ''
        self.LNP_sn = ''
        self.controller_name = ''
        self.controller_sn = ''


    def addPage(self, title=None):
        """"""
        panel = WizardPage(self, title)
        self.panelSizer.Add(panel, 2, wx.EXPAND)
        self.pages.append(panel)
        if len(self.pages) > 1:
            # hide all panels after the first one
            panel.Hide()
            self.Layout()

    def validateStageInfo(self, event):
        res = True
        self.validationMessage = ''
        # Stage name
        if self.chkStageName.Value:
            # Check box checked means everything is fine
            self.stage_name = self.labelStageNameValue.Label
        elif self.labelStageNameCorrectedValue.Value != '':
            # If the corrected stage name is filled, use it as the stage name
            self.stage_name = self.labelStageNameCorrectedValue.Value
            #TODO could use a format checker here
        else:
            # If the check box is not checked and the text control not filled: report an error
            res = False
            self.validationMessage += 'Verify the stage name\n'

        # Stage serial number
        if self.chkStageSN.Value:
            self.stage_sn = self.labelStageSNValue.Label
        elif self.labelStageSNCorrectedValue.Value != '':
            self.stage_sn = self.labelStageSNCorrectedValue.Value
        else:
            res = False
            self.validationMessage += 'Verify the stage serial number\n'

        # Controller
        if self.chkControllerName.Value:
            self.controller_name = self.labelControllerNameValue.Label
        elif self.labelControllerNameCorrectedValue.Value != '':
            self.controller_name = self.labelControllerNameCorrectedValue.Value
        else:
            res = False
            self.validationMessage += 'Verify the controller name\n'

        # Controller serial number
        if self.chkControllerSN.Value:
            self.controller_sn = self.labelControllerSNValue.Label
        elif self.labelControllerSNCorrectedValue.Value != '':
            self.controller_sn = self.labelControllerSNCorrectedValue.Value
        else:
            res = False
            self.validationMessage += 'Verify the controller serial number\n'

        # LNP serial number
        if self.labelLNPSNValue.Value == '':
            res = False
            self.validationMessage += 'Provide a serial number for the LNP or type none\n'
        else:
            self.LNP_sn = self.labelLNPSNValue.Value

        # Pressure
        if self.labelPressureValue.Value == '':
            res = False
            self.validationMessage += 'Provide a value for the test chamber pressure\n'

        # Affair
        if self.labelAffaireValue.Value == '':
            res = False
            self.validationMessage += 'Provide an Affair No.'


        if res:
            dlg = wx.MessageDialog(self, 'Press Next to continue.',
                   'Data confirmed!',
                   wx.OK | wx.ICON_INFORMATION
                   #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                   )
            dlg.ShowModal()
            self.nextBtn.Enable()
            self.validateBtn.Hide()
            #dlg.Destroy()
            self.nextBtn.Enable()
        else:
            dlg = wx.MessageDialog(self, self.validationMessage,
                   'Verify the data!',
                   wx.OK | wx.ICON_INFORMATION
                   #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                   )
            dlg.ShowModal()
            #dlg.Destroy()

    def onNext(self, event):
        """"""
        pageCount = len(self.pages)
        logger.debug('Going from page %d to %d' % (self.page_num, self.page_num+1))
        if self.opmode == 'test':
            self.authenticated = True
            self.user_name = 'Test'


        if self.authenticated:
            if self.nextBtn.GetLabel() == "Finish":
                logger.debug('Creating test report')
                report.go(self.dataContainer.logFile)
                # close the app
                self.GetParent().Destroy()

            elif pageCount-1 != self.page_num:
                if self.page_num == 1:
                    self.pages[self.page_num].Hide()
                    self.page_num += 1
                    self.pages[self.page_num].Show()
                    self.panelSizer.Layout()
                    self.prevBtn.Enable()
                    self.pageAction()

                else:
                    self.pages[self.page_num].Hide()
                    self.page_num += 1
                    self.pages[self.page_num].Show()
                    if self.page_num == 1:
                        self.validateBtn.Show()
                        self.mainSizer.Layout()
                    self.panelSizer.Layout()
                    self.prevBtn.Enable()
                    self.nextBtn.Disable()
                    self.pageAction()

            if pageCount -1 == self.page_num:
                # change label
                self.nextBtn.SetLabel("Finish")
        else:
            wx.MessageBox('Please log in before continuing', 'Error', wx.OK | wx.ICON_ERROR)
            pass # Do nothing if not authenticated

    def onPrev(self, event):
        """"""
        pageCount = len(self.pages)
        if self.page_num-1 != -1:
            self.pages[self.page_num].Hide()
            self.page_num -= 1
            self.pages[self.page_num].Show()
            self.panelSizer.Layout()
            self.nextBtn.Enable()
            if self.page_num == 0:
                self.prevBtn.Disable()
        #else:
        #    logger.debug("You're already on the first page!")

    def onAbort(self, event):
        logger.debug('Abort button pressed!')
        self.stop_test_event.set()
        # @TODO: implement clean abort of the linkam ramps!

    def onLogin(self, event):
        domain = "Xenocs.local"
        user_name = self.user.GetValue()
        user_password = self.passwd.GetValue()
        token = False
        logger.debug('Attempting to log in')
        os_name = platform.system()
        if os_name == 'Windows':
            try:
                logger.debug('logging in as %s' % user_name)
                token = win32security.LogonUser(
                                                user_name,
                                                domain,
                                                user_password,
                                                win32security.LOGON32_LOGON_NETWORK,
                                                win32security.LOGON32_PROVIDER_DEFAULT)
            except pywintypes.error:
                login_message = "User name or password is wrong! Try again!"
                self.login_status.SetLabelText(login_message)
                self.login_status.SetForegroundColour((128,0,0))
            self.authenticated = bool(token)
            if self.authenticated:
                win32security.ImpersonateLoggedOnUser(token)
                full_name = win32api.GetUserNameEx(3)
                login_message = "You are now logged in as %s. You can continue to start the test." % full_name
                self.login_status.SetLabelText(login_message)
                self.login_status.SetForegroundColour((0,128,0))
                self.user_name = full_name
        else:
            #try:
            # Login to ldap with linux
            server_ip = '172.22.1.1'
            server = ldap3.Server(server_ip, get_info=ldap3.ALL)
            user = '%s\\%s' % (domain,user_name)
            logger.info('user: %s' % user)
            conn = ldap3.Connection(server,
                                       user='%s\\%s' % (domain,user_name),
                                       password=user_password,
                                       authentication=ldap3.NTLM,
                                       auto_bind=True,
                                       auto_referrals=False)
            token = conn.bind()
            #except: 
            #    # Login credentials error handling
            #    logger.error('Connection error!')
            self.authenticated = bool(token)
            if self.authenticated:
                searchParameters = { 'search_base': 'dc=%s,dc=%s' % (domain.split('.')[0],domain.split('.')[1]),
                                     'search_filter': '(&(objectClass=Person)(sn=%s))' % (user_name),
                                     'attributes': ['displayName'],
                                   }
                conn.search(**searchParameters)
                self.user_name = conn.entries[0].displayName
                login_message = "You are now logged in as %s. You can continue to start the test." % self.user_name
                self.login_status.SetLabelText(login_message)
                self.login_status.SetForegroundColour((0,128,0))

    def createStep1(self):
        """Create step 1 panel"""
        logger.debug('createStep1')
        self.addPage("Step 1: connect stage")
        currPage = self.pages[0]
        label = """
                    This wizard will conduct an automated Linkam test procedure.
                    Make sure that:\n
                    - The Linkam stage is assembled and under vacuum (below 1 mBar)\n
                    - The liquid nitrogen dewar is full\n
                    - The Linkam controller and the liquid nitrogen pump are on\n
                    - The Linkam controller is connected to the test PC\n
                    \n
                    Pleas log in with your Xenocs user name and password to start the test.
                """
        info = wx.StaticText(currPage, label=label)
        currPage.addWidget(info, (2, 1), (1,6))

        user_lbl = wx.StaticText(currPage, label="Username:")
        currPage.addWidget(user_lbl, (7,2), (1,1))

        self.user = wx.TextCtrl(currPage)
        currPage.addWidget(self.user, (7,3), (1,1))

        pass_lbl = wx.StaticText(currPage, label="Password:")
        currPage.addWidget(pass_lbl, (9,2), (1,1))

        self.passwd = wx.TextCtrl(currPage, style=wx.TE_PASSWORD|wx.TE_PROCESS_ENTER)
        currPage.addWidget(self.passwd, (9,3), (1,1))

        self.login_status = wx.StaticText(currPage, label="")
        currPage.addWidget(self.login_status, (11,3), (1,3))

        btn = wx.Button(currPage, label="Login")
        btn.Bind(wx.EVT_BUTTON, self.onLogin)
        currPage.addWidget(btn, (9,4), (1,1))

    def createStep2(self):
        """Create step 2 panel"""
        self.addPage("Step 2: start test")
        currPage = self.pages[1]

        label = """
                Please check the information about the connected stage.
                - If the displayed name and serial number is correct, confirm by checking the corresponding check box on the left.
                - If the displayed value differs from the one marked on the stage or controller, please leave the check box unchecked
                  and fill the correct value in the text field to the right.

                Information about the LNP cannot be retrieved automatically - please fill out the serial number or type 'none'
                if there is no LNP connected.
                """
        info = wx.StaticText(currPage, label=label)
        currPage.addWidget(info, (2, 1), (1,6))

        # Stage name
        self.chkStageName = wx.CheckBox(currPage, label='Connected stage name: ')
        currPage.addWidget(self.chkStageName, (5,1), (1,1))
        self.labelStageNameValue = wx.StaticText(currPage, label='Default')
        currPage.addWidget(self.labelStageNameValue, (5,2), (1,1))
        self.labelStageNameCorrectedValue = wx.TextCtrl(currPage)
        currPage.addWidget(self.labelStageNameCorrectedValue, (5,3), (1,1))

        # Stage serial number
        self.chkStageSN = wx.CheckBox(currPage, label='Connected stage serial number: ')
        currPage.addWidget(self.chkStageSN, (6,1), (1,1))
        self.labelStageSNValue = wx.StaticText(currPage, label='Default')
        currPage.addWidget(self.labelStageSNValue, (6,2), (1,1))
        self.labelStageSNCorrectedValue = wx.TextCtrl(currPage)
        currPage.addWidget(self.labelStageSNCorrectedValue, (6,3), (1,1))

        # Controller
        self.chkControllerName = wx.CheckBox(currPage, label='Connected controller: ')
        currPage.addWidget(self.chkControllerName, (7,1), (1,1))
        self.labelControllerNameValue = wx.StaticText(currPage, label='Default')
        currPage.addWidget(self.labelControllerNameValue, (7,2), (1,1))
        self.labelControllerNameCorrectedValue = wx.TextCtrl(currPage)
        currPage.addWidget(self.labelControllerNameCorrectedValue, (7,3), (1,1))

        # Controller serial number
        self.chkControllerSN = wx.CheckBox(currPage, label='Connected controller serial number: ')
        currPage.addWidget(self.chkControllerSN, (8,1), (1,1))
        self.labelControllerSNValue = wx.StaticText(currPage, label='Default')
        currPage.addWidget(self.labelControllerSNValue, (8,2), (1,1))
        self.labelControllerSNCorrectedValue = wx.TextCtrl(currPage)
        currPage.addWidget(self.labelControllerSNCorrectedValue, (8,3), (1,1))

        # LNP serial number
        labelLNPSN = wx.StaticText(currPage, label='Connected LNP serial number: ')
        currPage.addWidget(labelLNPSN, (9,1), (1,1))
        self.labelLNPSNValue = wx.TextCtrl(currPage)
        currPage.addWidget(self.labelLNPSNValue, (9,2), (1,1))

        # Pressure
        labelPressure = wx.StaticText(currPage, label='Test chamber pressure [mbar]: ')
        currPage.addWidget(labelPressure, (10,1), (1,1))
        self.labelPressureValue = wx.TextCtrl(currPage)
        currPage.addWidget(self.labelPressureValue, (10,2), (1,1))

        # Affair
        labelAffaire = wx.StaticText(currPage, label='Affair No.: ')
        currPage.addWidget(labelAffaire, (11,1), (1,1))
        self.labelAffaireValue = wx.TextCtrl(currPage)
        currPage.addWidget(self.labelAffaireValue,(11,2),(1,1))

        # Comment
        labelComment = wx.StaticText(currPage, label='Comment: ')
        currPage.addWidget(labelComment, (12,1), (1,1))
        self.labelCommentValue = wx.TextCtrl(currPage)
        currPage.addWidget(self.labelCommentValue, (12, 2), (1,3))

        # Mechanic parts checkbox
        self.chkParts = wx.CheckBox(currPage, label = 'Part list verified - all parts present')
        currPage.addWidget(self.chkParts, (13,1), (1,1))

        # Save directory
        labelDir = wx.StaticText(currPage, label='Save the results to:')
        currPage.addWidget(labelDir, (15,1), (1,1))
        home_dir = str(pathlib.Path.home())
        self.dirPicker = wx.DirPickerCtrl(currPage, path=home_dir, size = (606, 26))
        logger.debug('DirPicker size:')
        logger.debug(self.dirPicker.Size)
        currPage.addWidget(self.dirPicker, (16,1), (1,8))


    def createStep3(self):
        """Create step 3 panel"""
        self.addPage("Step 3: testing")
        currPage = self.pages[2]
        # create the widgets
        label = """The test is now running. You can follow the progress in the window below.
                """
        info = wx.StaticText(currPage, label=label)
        currPage.addWidget(info, (2, 1), (1,6))

        self.log = wx.TextCtrl(currPage, wx.ID_ANY, size=(600,400),
                          style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        # Trying to fix Issue #1
        self.log.write = lambda *args: wx.CallAfter(self.log.AppendText, *args)

        currPage.addWidget(self.log, (3, 1), (9,18))
        self.labelCurrTemp = wx.StaticText(currPage, label='Current values: ')
        self.labelCurrTempValue = wx.StaticText(currPage, label='None')
        currPage.addWidget(self.labelCurrTemp, (13,1), (1,1))
        currPage.addWidget(self.labelCurrTempValue, (13,2), (1,1))


    def onUpdate(self, event):
        """
        """
        if len(self.dataContainer.data.keys()) > 0:
            label = ''
            for key in self.dataContainer.data.keys():
                value = self.dataContainer.data[key][-1]
                label += ' %s: %.2f |' % (key,value)
            self.labelCurrTempValue.SetLabel(label)
            #wx.Yield()
            try:
                if not self.test_thread.isAlive():
                    self.nextBtn.Enable()
            except AttributeError:
                pass


    def pageAction(self):
        logger.debug('pageAction called')
        if self.page_num == 1:
            logger.debug('Page 1 action')
            logger.debug('Setup test')
            try:
                logger.debug('Trying to get a coonection with the stage')
                #HOST = "192.168.0.110"
                #HOST = "172.22.11.46"
                #HOST = "192.168.0.111"
                HOST = "localhost"
                if self.opmode == 'test':
                    logger.debug('Running in test mode! Using dummy Linkam stage.')
                    self.stage = linkam.DummyStage()
                else:
                    stage_type = linkam.get_stage_type(HOST, 55555)
                    self.stage = linkam.known_stages[stage_type](HOST, 55555)#linkam.MyStage('localhost',55555)
                self.labelStageNameValue.SetLabel(self.stage.stage_name)
                self.labelStageSNValue.SetLabel(self.stage.stage_sn)
                self.labelControllerNameValue.SetLabel(self.stage.controller_name)
                self.labelControllerSNValue.SetLabel(self.stage.controller_sn)
                self.connectionStatus = 1
                self.validateBtn.Show()
            except Exception as e:
                logger.debug('Error!')
                notConnected = 'No stage connected!'
                self.labelStageNameValue.SetLabel(notConnected)
                self.labelStageSNValue.SetLabel(notConnected)
                self.labelControllerNameValue.SetLabel(notConnected)
                self.labelControllerSNValue.SetLabel(notConnected)
                self.nextBtn.Disable()
                dlg = wx.MessageDialog(self, 'Could not connect to the stage: %s.' % str(e),
                               'Something went wrong!',
                               wx.OK | wx.ICON_INFORMATION
                               #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                               )
                dlg.ShowModal()
                dlg.Destroy()
        elif self.page_num == 2:
            logger.debug('Page 2 action')
            logger.debug('Add handler for logger')
            handler = WxTextCtrlHandler(self.log)
            logger.debug('Handler added')
            handler.setFormatter(formatter)
            logger.handlers = [handler, fh]
            logger.info('Starting tests')
            # Save test details to the log file
            logger.info('Tester name: %s' % self.user_name)
            logger.info("Stage name: %s" % self.stage_name)
            logger.info("Stage serial number: %s" % self.stage_sn)
            logger.info("Controller name: %s" % self.controller_name)
            logger.info("Controller serial number: %s" % self.controller_sn)
            logger.info('Connected LNP serial number: %s' % self.LNP_sn)
            logger.info('Sample chamber pressure: %s mbar' % self.labelPressureValue.Value)
            logger.info('Hardware validated: %s' % self.chkParts.Value)
            logger.info('Affaire no: %s' % self.labelAffaireValue.Value)
            logger.info('Tester comment: %s' % self.labelCommentValue.Value)
            self.nextBtn.Disable()
            # Initialize the data container object
            self.dataContainer.outFile = self.generateFileName()
            # Prepare the test thread and start the stage test
            self.test_thread = threading.Thread(target=self.stage.test, args=(self.dataContainer, [handler, fh, ch], self.stop_test_event))
            self.test_thread.daemon = True
            self.test_finished_thread = threading.Thread(target=self.onTestFinish, args=(self.stop_test_event,))
            self.test_finished_thread.start()
            self.test_thread.start()
        #elif self.page_num == 3:
        #    self.test_thread.join()
        #    self.test_finished_thread.join()
            #logger.debug('Creating test report')
            #report.go(log_file)


    def onTestFinish(self,e):
        '''Code executed when the test is finished
        '''
        test_finish_event = e.wait()
        logger.debug('Test finished event set!')
        log_file = self.dataContainer.logFile
        #logger.debug('Creating test report')
        #report.go(log_file)
        #logger.debug('Done! Closing the program!')
        #self.GetParent().Close()

    def generateFileName(self):
        affair_dir = self.dirPicker.GetPath()
        fileName = affair_dir+'%s%s_%s.csv' % (os.path.sep,self.stage.stage_name, strftime('%Y-%m-%d-%H%M%S'))
        logger.info('Data will be saved to %s' % fileName)
        return fileName


class MainFrame(wx.Frame):
    """"""

    def __init__(self,opmode):
        """Constructor"""
        if opmode != None:
            logger.debug('Operation mode: %s' % opmode) 
        else:
            opmode='normal'
        self.user_name = 'none'
        wx.Frame.__init__(self, None, title="Linkam acceptance test", size=(800,600))
        self.panel = WizardPanel(self, opmode=opmode)
        self.panel.createStep1()
        self.panel.createStep2()
        self.panel.createStep3()
        self.SetIcon(icons.Xenocs.GetIcon())
        self.Show()

def is_valid_mode(parser, arg):
    if arg not in ['test']:
        parser.error('Mode %s is not implemented!' % arg)
    else:
        return arg

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Linkam stage automate acceptance tests')
    parser.add_argument('-m', dest='opmode', required=False,
            help='use <test> for simulated linkam stage connection', metavar='MODE',
            type=lambda x: is_valid_mode(parser, x))
    args = parser.parse_args()
    app = wx.App(False)
    frame = MainFrame(opmode=args.opmode)
    app.MainLoop()
