echo off
title Automated Linkam tests
echo Running Linkam server
start C:\Xenocs\Linkam\bin\linkam.bat
echo Activating python environment
set root= C:\ProgramData\Anaconda3
call C:\ProgramData\Anaconda3\Scripts\activate.bat %root%
echo Starting the test application
call python pylinkam.py
pause
