'''
Created on 13 avr. 2018

@author: Kwasniewski
'''
import csv
import pylab as p
import matplotlib.dates as md
from matplotlib.pyplot import subplots_adjust
import matplotlib.pyplot as plt
from datetime import datetime
import re
import pandas as pd
from pandas import plotting
import logging
from time import strftime
import tempfile
import matplotlib.dates as mdates
import os
import argparse

# create logger
logger = logging.getLogger('evaluate_results')
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create a file handler
tmp_dir = tempfile.gettempdir()
linkam_logfile = '%s%slinkam_%s.log' % (tmp_dir,os.path.sep,strftime('%Y-%m-%d-%H%M%S'))
fh = logging.FileHandler(linkam_logfile)
fh.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch and fh
ch.setFormatter(formatter)
fh.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)



def dateparse (time_in_secs):
    return datetime.fromtimestamp(float(time_in_secs))

class ResultAnalyser(object):
    '''A class defining a ResultAnalyser object, containing methods to parse the log file created by the automated linkam test
    procedure and prepare the data for automated report generation.
    '''
    def __init__(self, resultFilePath, resultFileHeader):
        '''The constrictor takes a string containing the path to the result file, including the
        file header, but withouth the extension.
        '''
        self.figure_file = 'None'
        self.figure_file_2 = 'None'
        self.resultFilePath = resultFilePath + resultFileHeader+'.csv'
        self.logFilePath = resultFilePath + resultFileHeader+'.log'
        # Load the data
        self.data = pd.read_csv(self.resultFilePath, header=0, parse_dates=[0],date_parser=dateparse,)
        self.data['Elapsed time [s]'] = self.data.loc[:,'Timestamp'] - self.data.loc[:,'Timestamp'][0]
        # Parse the log file
        logList = []
        with open(self.logFilePath, 'r') as f:
            logList.append(list(self.generateDicts(f)))
        self.logItems = pd.DataFrame.from_dict(logList[0])
        self.testSteps = None
        self.analyseLog()

    def generateDicts(self,log_fh):
        '''Constructs a dictionary storing the log file data
        '''
        currentDict = {}
        for line in log_fh:
            if self.matchDate(line):
                if currentDict:
                    yield currentDict

                currentDict = {"date":self.currDateTime,
                               "type":line.split("-",5)[4],
                               "message":line.split("-",5)[-1].strip('\n')}
            else:
                currentDict["message"] += line
        yield currentDict

    def matchDate(self, line):
        '''Uses a regular expression to match a date string, directly converting it to a datetime object
        '''
        matchThis = None
        date_re = re.compile('(?P<a_year>\d{2,4})-(?P<a_month>\d{2})-(?P<a_day>\d{2}) (?P<an_hour>\d{2}):(?P<a_minute>\d{2}):(?P<a_second>\d{2}),(?P<milliseconds>\d{3})')
        matched = date_re.match(line)
        if matched is not None:
            #matches a date and adds it to matchThis
            matchThis = matched.groupdict()
            self.currDateTime = datetime(int(matchThis['a_year']),
                                         int(matchThis['a_month']),
                                         int(matchThis['a_day']),
                                         int(matchThis['an_hour']),
                                         int(matchThis['a_minute']),
                                         int(matchThis['a_second']),
                                         int(matchThis['milliseconds'])*1000
                                         )
            return True
        else:
            return False
    def extractLogItem(self,keyword):
        try:
            value = self.logItems[self.logItems['message'].str.contains(keyword)]['message'].values[0].split(':')[-1].strip()
        except:
            value = 'Not found in the log file'
        return value

    def getStageInfo(self):
        '''Extracts the stage info (names, serial numbers) from the log
        '''
        self.tester_name = self.extractLogItem('Tester name') #self.logItems[self.logItems['message'].str.contains('Tester name')]['message'].get_values()[0].split(':')[-1].strip()
        self.stage_name = self.extractLogItem('Stage name')#self.logItems[self.logItems['message'].str.contains('Stage name')]['message'].get_values()[0].split(':')[-1].strip()
        self.stage_sn = self.extractLogItem('Stage serial')#self.logItems[self.logItems['message'].str.contains('Stage serial')]['message'].get_values()[0].split(':')[-1].strip()
        self.controller_name = self.extractLogItem('Controller name')#self.logItems[self.logItems['message'].str.contains('Controller name')]['message'].get_values()[0].split(':')[-1].strip()
        self.controller_sn = self.extractLogItem('Controller serial') #self.logItems[self.logItems['message'].str.contains('Controller serial')]['message'].get_values()[0].split(':')[-1].strip()
        self.lnp_sn = self.extractLogItem('LNP serial')#self.logItems[self.logItems['message'].str.contains('LNP serial')]['message'].get_values()[0].split(':')[-1].strip()
        self.pressure = self.extractLogItem('Sample chamber pressure')
        self.hardware_validated = self.extractLogItem('Hardware')
        self.affair_no = self.extractLogItem('Affair')
        self.comment = self.extractLogItem('Tester comment')
        logger.debug(self.tester_name)
        logger.debug(self.stage_name)
        logger.debug(self.stage_sn)
        logger.debug(self.controller_name)
        logger.debug(self.controller_sn)
        logger.debug(self.lnp_sn)

    def analyseLog(self):
        '''Performs the analysis of the log file, creating objects ready to use in the report
        '''
        self.getStageInfo()
        startIndex = self.logItems.loc[self.logItems['message'].str.contains('Starting the test')].index[0]
        stopIndex = self.logItems.loc[self.logItems['message'].str.contains('Test protocol finished!')].index[0]
        self.stageTestLog = self.logItems.loc[startIndex:stopIndex]
        # Execute the appropriate routine, depending on the stage type
        if ('HFS' in self.stage_name):
            self._analyseHFSX350()
        elif ('TST250' in self.stage_name):
            self._analyseTST250()
        elif ('MFS' in self.stage_name):
            self._analyseTST250()
        elif ('TS1000' in self.stage_name):
            self._analyseTS1000()
        elif ('Dummy' in self.stage_name):
            self._analyseDummy()
        else:
            raise TypeError('Method to analyze %s is not defined!' % self.stage_name)
    
    def _analyseDummy(self):
        '''Dummy test analysis
        '''
        logger.debug('_analyseDummy called')
        self.testSteps = self.stageTestLog.loc[self.stageTestLog['message'].str.contains('Dummy temperature')]
        test_bounds = []
        for item in self.testSteps.index:
            test_bounds.append(item)
        c = 0
        self.resultTable = [['Step', 'Description', 'Result', 'Comment']]
        for ind in test_bounds:
            #print(self.testSteps.loc[ind:ind+2])
            #print(' INFO ' in self.testSteps.loc[ind:ind+2].type.values)
            try:
                message_types = self.stageTestLog.loc[ind:test_bounds[c+1]].type.values
            except IndexError:
                message_types = self.stageTestLog.loc[ind:].type.values
            if ' ERROR ' in message_types:
                result = 'Failed'
                comment = self.stageTestLog.loc[ind+p.where(message_types == ' ERROR ')[-1]+1].message.values[0]
            else:
                result = 'Passed'
                comment = ''
            self.resultTable.append([c+1,self.stageTestLog.loc[ind].message.strip(), result, comment])
            c += 1
        logger.debug('Plotting the data...')
        fig = p.figure()
        ax = fig.add_subplot(111)
        #self.data.plot(ax=ax)
        self.plot_multi(self.data, cols=['Temperature [deg C]'], figsize=(12, 7),ax=ax)
        ax.set_xlabel('Timestamp')
        ax.set_ylabel(r'Temperature [$^{\circ}$C]')
        ax.set_title('Test run')
        p.legend([self.stage_name])
        logger.debug('Done! Saving...')
        self.figure_file = tempfile.gettempdir()+os.path.sep+'tmp.png'
        p.savefig(self.figure_file,dpi=300)
        logger.debug('Done! _analyseDummy has finished!')

    def _analyseHFSX350(self):
        '''Steps specific for the HFSX350 stage
        '''
        logger.debug('_analyseHFSX350 called')
        self.testSteps = self.stageTestLog.loc[self.stageTestLog['message'].str.contains('Temperature ramp from')]
        test_bounds = []
        for item in self.testSteps.index:
            test_bounds.append(item)
        c = 0
        self.resultTable = [['Step', 'Description', 'Result', 'Comment']]
        for ind in test_bounds:
            #print(self.testSteps.loc[ind:ind+2])
            #print(' INFO ' in self.testSteps.loc[ind:ind+2].type.values)
            try:
                message_types = self.stageTestLog.loc[ind:test_bounds[c+1]].type.values
            except IndexError:
                message_types = self.stageTestLog.loc[ind:].type.values
            if ' ERROR ' in message_types:
                result = 'Failed'
                comment = self.stageTestLog.loc[ind+p.where(message_types == ' ERROR ')[-1]+1].message.values[0]
            else:
                result = 'Passed'
                comment = ''
            self.resultTable.append([c+1,self.stageTestLog.loc[ind].message.strip(), result, comment])
            c += 1
        fig = p.figure()
        ax = fig.add_subplot(111)
        #self.data.plot(ax=ax)
        self.plot_multi(self.data, cols=['Temperature [deg C]'], figsize=(12, 7),ax=ax)
        ax.set_xlabel('Timestamp')
        ax.set_ylabel(r'Temperature [$^{\circ}$C]')
        ax.set_title('Test run')
        p.legend([self.stage_name])
        self.figure_file = tempfile.gettempdir()+os.path.sep+'tmp.png'
        p.savefig(self.figure_file,dpi=300)

    def _analyseTS1000(self):
        '''Steps specific for the TS1000 stage
        '''
        logger.debug('_analyseTS1000 called')
        self.testSteps = self.stageTestLog.loc[self.stageTestLog['message'].str.contains('Temperature ramp from')]
        test_bounds = []
        for item in self.testSteps.index:
            test_bounds.append(item)
        c = 0
        self.resultTable = [['Step', 'Description', 'Result', 'Comment']]
        for ind in test_bounds:
            #print(self.testSteps.loc[ind:ind+2])
            #print(' INFO ' in self.testSteps.loc[ind:ind+2].type.values)
            try:
                message_types = self.stageTestLog.loc[ind:test_bounds[c+1]].type.values
            except IndexError:
                message_types = self.stageTestLog.loc[ind:].type.values
            if ' ERROR ' in message_types:
                result = 'Failed'
                comment = self.stageTestLog.loc[ind+p.where(message_types == ' ERROR ')[-1]+1].message.values[0]
            else:
                result = 'Passed'
                comment = ''
            self.resultTable.append([c+1,self.stageTestLog.loc[ind].message.strip(), result, comment])
            c += 1
        fig = p.figure()
        ax = fig.add_subplot(111)
        #self.data.plot(ax=ax)
        self.plot_multi(self.data, cols=['Temperature [deg C]'], figsize=(12, 7),ax=ax)
        ax.set_xlabel('Timestamp')
        ax.set_ylabel(r'Temperature [$^{\circ}$C]')
        ax.set_title('Test run')
        p.legend([self.stage_name])
        self.figure_file = tempfile.gettempdir()+os.path.sep+'tmp.png'
        p.savefig(self.figure_file,dpi=300)

    def _analyseMFS(self):
        '''Steps specific for the MFS stage
        '''
        logger.debug('_analyseMFS called')
        self.testSteps = self.stageTestLog.loc[self.stageTestLog['message'].str.contains('TST communication test')]
        self.tempTestSteps = self.stageTestLog.loc[self.stageTestLog['message'].str.contains('Temperature ramp from')]
        self.tensileComplete = self.stageTestLog.loc[self.stageTestLog['message'].str.contains('Tensile stage communication tests completed')].date.values[0]
        logger.debug("Tensile test completed: %s" % self.tensileComplete)
        logger.debug("type(self.tensileComplete): %s" % type(self.tensileComplete))
        test_bounds = []
        for item in self.testSteps.index:
            test_bounds.append(item)
        for item in self.tempTestSteps.index:
            test_bounds.append(item)
        c = 0
        self.resultTable = [['Step', 'Description', 'Result', 'Comment']]
        for ind in test_bounds:
            #print(self.testSteps.loc[ind:ind+2])
            #print(' INFO ' in self.testSteps.loc[ind:ind+2].type.values)
            try:
                message_types = self.stageTestLog.loc[ind:test_bounds[c+1]].type.values
            except IndexError:
                message_types = self.stageTestLog.loc[ind:].type.values
            if ' ERROR ' in message_types:
                result = 'Failed'
                comment = self.stageTestLog.loc[ind+p.where(message_types == ' ERROR ')[-1]].message.values[0]
            else:
                result = 'Passed'
                comment = ''
            self.resultTable.append([c+1,self.stageTestLog.loc[ind].message.strip(), result, comment])
            c += 1
        '''
        fig = p.figure()
        ax = fig.add_subplot(111)
        self.data.plot(x = 'Timestamp', y = 'TST position [mm]', ax=ax)
        self.data.plot(x = 'Timestamp', y = 'Temperature [deg C]', ax=ax, secondary_y=True, style='g')
        ax.set_xlabel('Time')
        ax.set_ylabel(r'TST position [mm]')
        ax.set_title('Test run')
        p.legend([self.stage_name])
        '''
        logger.debug("type(self.data['Timestamp'].values[0]): %s" % type(self.data['Timestamp'].values[0]))
        fig1 = p.figure(figsize=(12,7))
        fig2 = p.figure(figsize=(12,7))
        ax1 = fig1.add_subplot(111)
        ax2 = fig2.add_subplot(111)
        self.plot_multi(self.data, cols=['Temperature [deg C]', 'Force [N]', 'TST position [mm]'], ax=ax1)
        fig1.subplots_adjust(right=0.7)
        self.plot_multi(self.data, cols=['Temperature [deg C]'], ax=ax2)
        ax1.set_xlim(self.data['Timestamp'].values[0], self.tensileComplete)
        ax2.set_xlim(self.tensileComplete, self.data['Timestamp'].values[-1])
        self.figure_file = tempfile.gettempdir()+os.path.sep+'tmp.png'
        self.figure_file_2 = tempfile.gettempdir()+os.path.sep+'tmp2.png'
        logger.debug('Figure temp file: %s' % self.figure_file)
        fig1.savefig(self.figure_file,dpi=300)
        fig2.savefig(self.figure_file_2,dpi=300)

    def _analyseTST250(self):
        '''Steps specific for the TST250 stage
        '''
        logger.debug('_analyseTST250 called')
        self.testSteps = self.stageTestLog.loc[self.stageTestLog['message'].str.contains('TST communication test')]
        self.tempTestSteps = self.stageTestLog.loc[self.stageTestLog['message'].str.contains('Temperature ramp from')]
        self.tempTestStart = self.stageTestLog.loc[self.stageTestLog['message'].str.contains('Starting the temperature test protocol')].index[0]
        logger.info("Real temp test starts at %d" % self.tempTestStart)
        self.tensileComplete = self.stageTestLog.loc[self.stageTestLog['message'].str.contains('Tensile stage communication tests completed')].date.values[0]
        logger.debug("Tensile test completed: %s" % self.tensileComplete)
        #logger.debug("type(self.tensileComplete): %s" % type(self.tensileComplete))
        test_bounds = []
        for item in self.testSteps.index:
            test_bounds.append(item)
            #logger.debug("tempStep: %s" % item)
        for item in self.tempTestSteps.index:
            test_bounds.append(item)
            #logger.debug("tempTestStep: %s" % item)
        c = 0
        self.resultTable = [['Step', 'Description', 'Result', 'Comment']]
        for ind in test_bounds:
            logger.info(ind)
            #print(self.testSteps.loc[ind:ind+2])
            #print(' INFO ' in self.testSteps.loc[ind:ind+2].type.values)
            try:
                message_types = self.stageTestLog.loc[ind:test_bounds[c+1]].type.values
            except IndexError:
                message_types = self.stageTestLog.loc[ind:].type.values
            #logger.debug("ind = %d, message_types = %s" % (ind, message_types))
            if ' ERROR ' in message_types:
                result = 'Failed'
                comment = self.stageTestLog.loc[ind+p.where(message_types == ' ERROR ')[-1]].message.values[0]
            else:
                result = 'Passed'
                comment = ''

            if ((ind in self.tempTestSteps.index) and (ind > self.tempTestStart)) or (ind in self.testSteps.index):
                self.resultTable.append([c+1,self.stageTestLog.loc[ind].message.strip(), result, comment])
            c += 1
        '''
        fig = p.figure()
        ax = fig.add_subplot(111)
        self.data.plot(x = 'Timestamp', y = 'TST position [mm]', ax=ax)
        self.data.plot(x = 'Timestamp', y = 'Temperature [deg C]', ax=ax, secondary_y=True, style='g')
        ax.set_xlabel('Time')
        ax.set_ylabel(r'TST position [mm]')
        ax.set_title('Test run')
        p.legend([self.stage_name])
        '''
        logger.debug("type(self.data['Timestamp'].values[0]): %s" % type(self.data['Timestamp'].values[0]))
        fig1 = p.figure(figsize=(12,7))
        fig2 = p.figure(figsize=(12,7))
        ax1 = fig1.add_subplot(111)
        ax2 = fig2.add_subplot(111)
        self.plot_multi(self.data, cols=['Temperature [deg C]', 'Force [N]', 'TST position [mm]'], ax=ax1)
        fig1.subplots_adjust(right=0.7)
        self.plot_multi(self.data, cols=['Temperature [deg C]'], ax=ax2)
        ax1.set_xlim(self.data['Timestamp'].values[0], self.tensileComplete)
        ax2.set_xlim(self.tensileComplete, self.data['Timestamp'].values[-1])
        self.figure_file = tempfile.gettempdir()+os.path.sep+'tmp.png'
        self.figure_file_2 = tempfile.gettempdir()+os.path.sep+'tmp2.png'
        logger.debug('Figure temp file: %s' % self.figure_file)
        fig1.savefig(self.figure_file,dpi=300)
        fig2.savefig(self.figure_file_2,dpi=300)

    def plot_multi(self, data, ax, cols=None, spacing=.1, **kwargs):
        # Get default color style from pandas - can be changed to any other color list
        if cols is None: cols = data.columns
        if len(cols) == 0: return
        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

        # First axis
        data.plot(x='Timestamp', y=cols[0], label=cols[0], color=colors[0],legend=False, ax=ax, **kwargs)
        plt.gcf().autofmt_xdate()
        plt.gcf().subplots_adjust(right=0.7)
        ax.set_ylabel(ylabel=cols[0])
        lines, labels = ax.get_legend_handles_labels()

        for n in range(1, len(cols)):
            # Multiple y-axes
            ax_new = ax.twinx()
            ax_new.spines['right'].set_position(('axes', 1 + spacing * (n - 1)))
            data.plot(x='Timestamp', y=cols[n], ax=ax_new, label=cols[n], color=colors[n % len(colors)],legend=False)
            ax_new.set_ylabel(ylabel=cols[n])

            # Proper legend position
            line, label = ax_new.get_legend_handles_labels()
            lines += line
            labels += label
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
        ax.legend(lines, labels, loc="upper left", bbox_to_anchor=(1.2,1))
        #return ax


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Evaluate Linkam test results')
    parser.add_argument('dir', help='Directory with the test results')
    parser.add_argument('file_header', help='File name (without the extension) of the results to analyze')

    args = parser.parse_args()

    a = ResultAnalyser(args.dir,args.file_header)

    #a = ResultAnalyser('C:\\Xenocs\\', 'MFS350V_2020-06-16-190135')
    #print(a.testSteps)
    '''
    fig = p.figure()
    ax = fig.add_subplot(111)
    a.data.plot(ax=ax)
    p.show()
    '''
