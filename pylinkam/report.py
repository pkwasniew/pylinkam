from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.colors import red, green
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import inch, mm, cm
from reportlab.lib import utils
from reportlab.platypus.flowables import Image
from reportlab.platypus import Frame
from time import strftime
from evaluate_results import ResultAnalyser
from pathlib import PurePath
import os
import argparse
import logging
import tempfile

# create logger
logger = logging.getLogger('report')
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create a file handler
tmp_dir = tempfile.gettempdir()
linkam_logfile = '%s%slinkam_%s.log' % (tmp_dir,os.path.sep,strftime('%Y-%m-%d-%H%M%S'))
fh = logging.FileHandler(linkam_logfile)
fh.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch and fh
ch.setFormatter(formatter)
fh.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

PAGE_HEIGHT=A4[1]
PAGE_WIDTH=A4[0]
styles = getSampleStyleSheet()
Title = "Hello world"
pageinfo = "Linkam test report"
logo = 'Xenocs_logo.jpg'

Xenocs_blue = '#001E60'
Xenocs_gray = '#8A8D8F'

def get_image(path, width=1*inch):
    img = utils.ImageReader(path)
    iw, ih = img.getSize()
    aspect = ih / float(iw)
    resImg = Image(path, width=width, height=(width * aspect))
    return resImg

def myPageHeader(canvas, doc):
    # Header
    # Centered logo
    hl_width = 8 * cm
    hl_height = PAGE_HEIGHT/10 - cm
    hl_x = PAGE_WIDTH/2 - hl_width/2
    hl_y = PAGE_HEIGHT-PAGE_HEIGHT/10
    header_logo = Frame(hl_x, hl_y, hl_width, hl_height, showBoundary=0)
    story = []
    logo_img = get_image(logo, width=6*cm)
    logo_img.hAlign = 'CENTER'
    story.append(logo_img)
    header_logo.addFromList(story, canvas)

    # Template title
    ht_width = 5 * cm
    ht_height = cm
    ht_x = 1 * cm
    ht_y = hl_y
    header_text = Frame(ht_x, ht_y, ht_width, ht_height, showBoundary=0)
    story = []
    style = styles["Normal"]
    style.textColor = Xenocs_blue
    story.append(Paragraph('<b>LINKAM TEST REPORT</b>', style))
    header_text.addFromList(story, canvas)

    # Current date
    hd_width = 5 * cm
    hd_height = cm
    hd_x = PAGE_WIDTH - hd_width - cm
    hd_y = hl_y
    header_date = Frame(hd_x, hd_y, hd_width, hd_height, showBoundary=0)
    date_txt = strftime('%Y-%m-%d')
    story = []
    style = styles["Normal"]
    style.textColor = Xenocs_blue
    style.alignment = 2
    story.append(Paragraph(date_txt, style))
    header_date.addFromList(story, canvas)


def myFirstPage(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Bold',16)
    #canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-72, Title)
    canvas.setFont('Times-Roman',9)
    canvas.drawString(10 * mm, 10 * mm,"Page %d; %s" % (doc.page, pageinfo))
    myPageHeader(canvas, doc)
    canvas.restoreState()

def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Roman', 9)
    canvas.drawString(10 * mm, 10 * mm,"Page %d; %s" % (doc.page, pageinfo))
    myPageHeader(canvas, doc)
    canvas.restoreState()

def go(resultFile):
    logger.debug(r'Calling go with %s ' % resultFile)
    pathObject = PurePath(resultFile)
    resultFilePath = str(pathObject.parent) + os.path.sep
    resultFileHeader = pathObject.stem
    logger.debug('Starting ResultAnalyser')
    a = ResultAnalyser(resultFilePath,resultFileHeader)
    logger.debug('ResultAnalyser finished')
    logger.debug('Building the report')
    doc = SimpleDocTemplate(resultFilePath + resultFileHeader + ".pdf", pagesize=A4,
                            rightMargin=36,leftMargin=36,
                            topMargin=4 * cm,bottomMargin=36)
    Story = [Spacer(1,10*mm)]
    style = styles["Normal"]
    subset = a.testSteps[['message']]
    tuples = [tuple(x) for x in subset.values]
    stage_info = [['Tester name', a.tester_name],
                  ['Stage type', a.stage_name],
                  ['Stage serial number', a.stage_sn],
                  ['Controller', a.controller_name],
                  ['Controller serial number', a.controller_sn],
                  ['LNP serial number', a.lnp_sn],
                  ['Sample chamber pressure', a.pressure],
                  ['Hardware complete', a.hardware_validated],
                  ['Affair no.', a.affair_no],
                  ['Comment', a.comment]
                  ]
    table = Table(tuples)
    Story.append(Table(stage_info))
    Story.append(Spacer(1,10*mm))
    #Story.append(table)
    result_table = Table(a.resultTable)
    table_style = TableStyle([('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                              ('FONTSIZE', (0, 0), (-1, -1), 8)
                              ])
    for row, values, in enumerate(a.resultTable):
        for column, value in enumerate(values):
            if value == 'Failed':
                table_style.add('TEXTCOLOR', (column, row), (column, row), red)
            elif value == 'Passed':
                table_style.add('TEXTCOLOR', (column, row), (column, row), green)
    result_table.setStyle(table_style)
    Story.append(result_table)
    logger.debug('Results table added to report')

    data_plot = get_image(a.figure_file,width=19*cm)
    Story.append(data_plot)
    if a.figure_file_2 != 'None':
        Story.append(get_image(a.figure_file_2,width=19*cm))
    logger.debug('Plots added to report')
    logger.debug('Building the story...')
    doc.build(Story, onFirstPage=myFirstPage, onLaterPages=myLaterPages)
    logger.debug('Done!')

def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error('The file %s does not exist!' % arg)
    else:
        return arg

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Make report from test log.')
    parser.add_argument('-i', dest='filename', required=True,
            help='input log file', metavar='FILE',
            type=lambda x: is_valid_file(parser, x))
    args = parser.parse_args()
    go(args.filename)
