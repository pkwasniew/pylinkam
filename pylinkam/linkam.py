'''
Created on 3 avr. 2018

@author: Kwasniewski
'''

import socket
from time import sleep, time
import threading
import logging
from numpy import abs
import argparse
from random import random

# create logger
logger = logging.getLogger('linkam')
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class TemperatureRateError(Error):
    """Exception raised for temperature rate errors.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

class CommandNotDefinedError(Error):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)

def get_stage_type(HOST, PORT):
    '''Returns a string describing the stage connected
    '''
    logger.debug('Trying to get stage type')
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        cmd = ('get_stage_name')
        logger.debug('Opening socket')
        s.connect((HOST, PORT))
        logger.debug('Socket open!')
        s.sendall(b'%b\r' % str.encode(cmd))
        reply = s.recv(1024).strip()
    return reply.decode()

class MyStage(object):
    '''
    Class defining the Linkam stage. Defines functions available for all Linkam stages
    '''

    def __init__(self, HOST, PORT):
        '''
        Constructor
        '''
        self.HOST = HOST
        self.PORT = PORT
        self.controller_name = self._send_cmd("get_controller_name")
        self.controller_sn = self._send_cmd("get_controller_serial")
        self.stage_name = self._send_cmd("get_stage_name")
        self.stage_sn = self._send_cmd("get_stage_serial")
        self.td = 0.1 # delay time to wait between commands
        self._observers = []

    def _send_cmd(self,cmd):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.HOST, self.PORT))
            s.sendall(b'%b\r' % str.encode(cmd))
            reply = s.recv(1024).strip()
            out = reply.decode()
            if out == 'NOK: COMMAND DOES NOT EXIST':
                raise CommandNotDefinedError('Command %s does not exist!' % cmd)
        return out

    def _lkm_get_float(self, cmd):
        return float(self._send_cmd(cmd))

    # Get commands
    def get_temp(self):
        '''Return current temperature in deg C as a float number
        '''
        #logger.debug('get_temp called!')
        return self._lkm_get_float('get_temp')

    def get_temp_target(self):
        '''Return current temperature target in deg C as a float number
        '''
        return self._lkm_get_float("get_temp_limit")

    def get_rate(self):
        '''Return the currently set temperature rate in deg C/min
        '''
        return self._lkm_get_float("get_rate")

    # Set commands
    def set_temp_target(self, val: float):
        '''Set the target temperature
        '''
        logger.debug('Setting temperature target to %.2f' % val)
        self._send_cmd('set_temp_limit %f' % val)

    def set_rate(self, val: float):
        '''Set the temperature rate to val in deg/min
        '''
        logger.debug('Setting temperature change rate to %.2f' % val)
        self._send_cmd('set_rate %f' % val)


    # Start & stop commands
    def start_temp_ramp(self):
        logger.debug('Starting temperature ramp now!')
        self._send_cmd('start_heating')

    def stop_temp_ramp(self):
        logger.debug('Stopping temperature ramp now!')
        self._send_cmd('stop_heating')

    # Helper functions
    def print_stage_info(self):
        '''Print information about the connected stage
        '''
        print("Stage name: %s" % self.stage_name)
        print("Stage serial number: %s" % self.stage_sn)
        print("Controller name: %s" % self.controller_name)
        print("Controller serial number: %s" % self.controller_sn)

    def is_connected(self):
        return self._send_cmd('is_connected')

    def bind_to(self, callback):
        print('bound')
        self._observers.append(callback)

    def updateData(self):
        #logger.debug('updateData called')
        self.curr_temp = self.get_temp()
        self.dataContainer.updateData(self.curr_temp)

    def updateDaemon(self, delay, stop_event):
        starttime = time()
        logger.debug('updateDaemon started')
        while not stop_event.is_set():
            self.updateData()
            sleep(delay - ((time() - starttime) % delay))
        logger.debug('updateDaemon stopped')

    def waitOnCooling(self, stop_test_event):
        delta_temp = abs(self.curr_temp - self.get_temp_target())
        estimated_time = delta_temp / (self.get_rate() / 60.)
        logger.info('Estimated ramp time: %.2f s' % estimated_time)
        start_time = time()
        condition = True
        while condition and not stop_test_event.is_set():
            temp_not_achieved = self.curr_temp > self.get_temp_target() + self.temp_delta
            elapsed_time = time() - start_time
            if temp_not_achieved and (elapsed_time > 1.5 * estimated_time):
                error = TemperatureRateError('Time limit for reaching the set value exceeded. Something is wrong.')
                logger.error(error.message)
                raise error

                condition = False
            elif not temp_not_achieved:
                logger.info('Target temperature achieved after %.2f s' % elapsed_time)
                condition = False
            sleep(self.refresh_rate)

    def waitOnHeating(self, stop_test_event):
        delta_temp = abs(self.curr_temp - self.get_temp_target())
        estimated_time = delta_temp / (self.get_rate() / 60.)
        logger.debug('Estimated ramp time: %.2f s' % estimated_time)
        start_time = time()
        condition = True
        while condition and not stop_test_event.is_set():
            temp_not_achieved = self.curr_temp < self.get_temp_target() - self.temp_delta
            elapsed_time = time() - start_time
            if temp_not_achieved and (elapsed_time > 1.5 * estimated_time):
                raise TemperatureRateError('Time limit for reaching the set value exceeded. Something is wrong.')
                condition = False
            elif not temp_not_achieved:
                logger.info('Target temperature achieved after %.2f s' % elapsed_time)
                condition = False
            sleep(self.refresh_rate)

    def coolAndWait(self, temp_target, rate, wait_time, stop_test_event):
        logger.info('Temperature ramp from %.2f to %.2f deg C with %.2f deg / min' % (self.curr_temp, temp_target, rate))
        self.set_temp_target(temp_target)
        self.set_rate(rate)
        self.start_temp_ramp()
        self.waitOnCooling(stop_test_event)
        logger.info('Waiting for %.2f s' % wait_time)
        sleep(wait_time)

    def heatAndWait(self, temp_target, rate, wait_time, stop_test_event):
        logger.info('Temperature ramp from %.2f to %.2f deg C with %.2f deg / min' % (self.curr_temp, temp_target, rate))
        self.set_temp_target(temp_target)
        self.set_rate(rate)
        self.start_temp_ramp()
        self.waitOnHeating(stop_test_event)
        logger.info('Waiting for %.2f s' % wait_time)
        sleep(wait_time)

    def goAndWait(self, temp_target, rate, wait_time, stop_test_event):
        if temp_target > self.curr_temp:
            try:
                self.heatAndWait(temp_target, rate, wait_time, stop_test_event)
            except TemperatureRateError:
                logger.error('Temperature not reached within the expected time!')
                pass
        else:
            try:
                self.coolAndWait(temp_target, rate, wait_time, stop_test_event)
            except TemperatureRateError:
                logger.error('Temperature not reached within the expected time!')
                pass

    def _test_communication(self):
        '''Test of all the commands sent to the stage
        '''
        logger.info("Stage name: %s" % self.stage_name)
        logger.info("Stage serial number: %s" % self.stage_sn)
        logger.info("Controller name: %s" % self.controller_name)
        logger.info("Controller serial number: %s" % self.controller_sn)

class TensileStage(MyStage):
    '''Tensile stage object. Inherits the MyStage functions and adds TST specific ones.
    '''

    def get_stage_type(self):
        return 'tensile'
    # Get functions specific for the TST
    def get_tst_mode(self):
        return float(self._send_cmd('get_tst_mode'))

    def get_tst_position(self):
        return self._lkm_get_float('get_tst_position')

    def get_tst_force(self):
        return float(self._send_cmd('get_tst_force'))

    def get_tst_force_limit(self):
        '''Define the force set point in [N]
        '''
        return float(self._send_cmd('get_tst_force_limit'))

    def get_tst_motor_velocity(self):
        '''Return the current TST motor velocity in um/s
        '''
        return float(self._send_cmd('get_tst_motor_velocity'))

    def get_tst_motor_limit(self):
        '''Return the current TST motor position limit in um
        '''
        return float(self._send_cmd('get_tst_motor_limit'))

    # TST specific set functions
    def set_tst_mode(self, mode):
        '''Set the operation mode for the TST. The allowed values are "velocoity" or "force".
        '''
        mode_list = ['velocity', 'step', 'cycle', 'force']
        if mode not in mode_list:
            message = "\'%s\' is not a valid mode! Choose from %s" % (mode, mode_list)
            error = ValueError(message)
            logger.error(message)
            raise error
        else:
            self._send_cmd('set_tst_mode %s' % mode)
            sleep(self.td)

    def set_tst_force_limit(self, val: float):
        '''Define the force set point in [N]
        '''
        self._send_cmd('set_tst_force_limit %f' % val)
        sleep(self.td)

    def set_tst_motor_velocity(self, val: float):
        '''Set the TST motor velocity to val in um/s
        '''
        self._send_cmd('set_tst_motor_velocity %f' % val)
        sleep(self.td)

    def set_tst_motor_limit(self, val: float):
        '''Set the displacement value in [um]
        '''
        self._send_cmd('set_tst_motor_limit %f' % val)
        sleep(self.td)

    def get_tst_pid_kp(slef):
        return self._lkm_get_float('get_tst_pid_kp')

    def get_tst_pid_ki(slef):
        return self._lkm_get_float('lkm_get_tst_pid_ki')

    def get_tst_pid_kd(slef):
        return self._lkm_get_float('get_tst_pid_kd')

    def get_tst_force_gauge(self):
        return self._lkm_get_float('get_tst_force_gauge')

    def set_tst_direction(self, direction):
        '''Accepts open/close for direction
        '''
        if direction not in ['open', 'close']:
            message = 'Direction must be either \'open\' or \'close\', \'%s\' is not defined.' % direction
            error = ValueError(message)
            logger.error(message)
            raise error
        else:
            self._send_cmd('set_tst_table_direction %s' % direction)
            sleep(self.td)

    def get_tst_direction(self):
        return self._send_cmd("get_tst_table_direction")

    # TST specific start/stop functions
    def start_tst_motor(self):
        self._send_cmd('start_motor 5')

    def stop_tst_motor(self):
        self._send_cmd('stop_motor 5')

    def updateData(self):
        #logger.debug('updateData called')
        td = 0.00
        timestamp = time()
        self.curr_temp = self.get_temp()
        sleep(td)
        curr_force = self.get_tst_force()
        sleep(td)
        curr_position = self.get_tst_position()
        sleep(td)
        curr_vel = self.get_tst_motor_velocity()
        sleep(td)
        data = {'Timestamp': timestamp,
                'Temperature [deg C]': self.curr_temp,
                'Force [N]': curr_force,
                'TST position [mm]': curr_position,
                'TST velocity [microns/s]': curr_vel
                }
        self.dataContainer.updateData(data)

    def set_get_test(self,set_func,arg,get_func):
        set_func(arg)
        out = get_func()
        return out

    def _test_modes(self,mode_list):
        for item in mode_list:
            try: 
                self.set_tst_mode(item)
                logger.info('Operation mode changed successfully to %s' % item)
            except:
                logger.error('Error while trying to test operation mode \"%s\"' % item)


    def test(self,dataContainer,logger_handlers, stop_test_event):
        '''Acceptance test specific for the TST stage
        '''
        logger.handlers = logger_handlers
        self.dataContainer = dataContainer
        logger.debug('starting updateDeamon')
        stop_event = threading.Event()
        thread = threading.Thread(target=self.updateDaemon, args=(0.2,stop_event))
        thread.daemon = True
        thread.start()
        av_time = 5
        logger.info('Measuring ambient temperature for %.f s' % av_time)
        sleep(av_time)

        # Start the test protocol
        logger.info('Starting the test protocol')

        # First, test if the controller responds properly to set and get commands
        ######################
        # Communication test #
        ######################
        logger.info('Starting Tensile stage communication tests')
        #----------------#
        # Operation mode #
        #----------------#
        logger.info('TST communication test 1. Operation mode test')
        mode_list = ['velocity','force','step','cycle']
        self._test_modes(mode_list)

        #-------------#
        # Force limit #
        #-------------#
        logger.info('TST communication test 2. Force limit test')
        try:
            flim = 20.14
            self.set_tst_force_limit(flim)
            out = self.get_tst_force_limit()
            if out == flim:
                logger.info('Force limit changed successfully to %.2f' % flim)
            else:
                logger.error('Force limit set: %.2f, force limit get: %.2f' % (flim, out))
        except:
            logger.error('Problem encountered when trying to set force limit')

        #--------------------#
        # TST motor position #
        #--------------------#
        logger.info('TST communication test 3. Motor position and velocity')
        try:
            self._test_movement()
            #mode = 'step'
            #self.set_tst_mode(mode)
            #logger.info('Operation mode changed successfully to %s' % mode)
            #start_pos = self.get_tst_position()
            #start_vel = self.get_tst_motor_velocity()
            #start_limit = self.get_tst_motor_limit()
            #logger.info('Initial TST motor position: %.2f; velocity: %.2f; limit: %.2f' % (start_pos, start_vel, start_limit))
            #n_cycles = 10
            #amplitude = 4480.0
            #for i in range(n_cycles):
            #    curr_pos = self.get_tst_position()
            #    if not i % 2:
            #        new_limit =  2*amplitude
            #        velocity= 984.
            #        waiting_time = 8
            #    else:
            #        new_limit = amplitude
            #        velocity= 984./2
            #        waiting_time = 16
            #    self.set_tst_motor_velocity(velocity)
            #    self.set_tst_motor_limit(new_limit)
            #    logger.info('Moving the TST motor to %.1f microns with %.1f microns/s speed' % (new_limit,velocity))
            #    self.start_tst_motor()
            #    sleep(waiting_time)
            #    curr_pos = self.get_tst_position()
            #logger.info('TST motor moved back to %.2f' % curr_pos)
            logger.info('Motor position and velocity test completed')
        except:
            logger.error('Problem moving the TST motor')

        logger.info('Tensile stage communication tests completed')

        logger.info('Temperature control test')

        logger.info('Measuring ambient temperature for 10 s')
        sleep(10)
        rt = self.curr_temp
        logger.info('Results will be saved to %s' % self.dataContainer.outFile)
        # Set test parameters
        self.refresh_rate = 1 # [s] # How often is the temperature checked during ramps
        self.temp_delta = 0.2 # [deg C]
        stable_time = 60 # [s] # How long is the temperature kept after a ramp

        logger.info('Purging the LN tubing')
        # Ramp 1
        self.goAndWait(0, 30, stable_time, stop_test_event)
        # Ramp 2
        self.goAndWait(rt, 30, stable_time, stop_test_event)
        #self.stop_temp_ramp()
        logger.info('Starting the temperature test protocol')
        # Ramp 3
        self.goAndWait(0, 15, stable_time, stop_test_event)
        # Ramp 4
        self.goAndWait(-100, 30, stable_time, stop_test_event)
        # Ramp 5
        self.goAndWait(-150, 15, stable_time, stop_test_event)
        # Ramp 6
        #self.goAndWait(-190, 10, stable_time, stop_test_event)
        # Ramp 7
        self.goAndWait(-20, 30, stable_time, stop_test_event)
        # Ramp 8
        self.goAndWait(100, 30, stable_time, stop_test_event)
        # Ramp 9
        self.goAndWait(140, 15, stable_time, stop_test_event)
        # Ramp 10
        self.goAndWait(250, 30, stable_time, stop_test_event)
        # Ramp 11
        self.goAndWait(rt, 30, stable_time, stop_test_event)
        self.stop_temp_ramp()

        # Stop recording the temperature
        logger.info('Test protocol finished!')
        self.dataContainer.moveToFinalDir()
        stop_event.set()
        self.stop_tst_motor()
        self.stop_temp_ramp()
        stop_test_event.set()

    def _calc_time_to_limit(self,curr_pos,dir,vel):
        '''Calculate approximate time to reach limit'''
        if dir == 'open':
            limit_pos = 88000
        else:
            limit_pos = 0
        dist_to_limit = abs(limit_pos - curr_pos)
        time_to_limit = dist_to_limit/vel
        return 1.2 * time_to_limit + 2

    def _tst_go_to_limit(self,dir):
        start_pos = self.get_tst_position()
        logger.info('Current position is %.2f' % start_pos)
        vel = 4000
        logger.info('Choosing direction \'%s\'' % dir)
        self.set_tst_direction(dir)
        self.set_tst_mode('step')
        self.set_tst_mode('velocity')
        self.set_tst_motor_velocity(vel)
        self.start_tst_motor()
        sleep_time = self._calc_time_to_limit(start_pos,dir,vel)
        logger.info('Movement should take no more than %d s' % sleep_time)
        sleep(sleep_time)
        self.stop_tst_motor()
        sleep(1)
        curr_pos = self.get_tst_position()
        delta_pos = curr_pos - start_pos
        logger.info('Current position is %.2f' % curr_pos)

    def _tst_test_range(self):
        logger.info('Testing full movement range')
        self._tst_go_to_limit('open')
        limit1_pos = self.get_tst_position()
        self._tst_go_to_limit('close')
        limit2_pos = self.get_tst_position()
        tst_range = abs(limit2_pos - limit1_pos)
        logger.info('Measured travel range: %.2f um' % tst_range)

    def _test_movement(self):
        '''Test of jaw movement and availale modes
        '''
        logger.info("Testing step mode")
        step_val = [5000,4000,1000,35,30000]
        dir_val = ['open', 'close']
        vel_val = [5000,4000,1000,100,5000]
        self.set_tst_mode('step')
        for i in range(len(step_val)):
            step = step_val[i]
            vel = vel_val[i]
            dir = dir_val[i%2]
            sleep_time = step/vel + 5
            logger.info('Setting step value to %d um, velocity to %d um/s and direction to %s' % (step, vel, dir))
            start_pos = self.get_tst_position()
            start_vel = self.get_tst_motor_velocity()
            self.set_tst_motor_limit(step)
            self.set_tst_direction(dir)
            self.set_tst_motor_velocity(vel)
            self.start_tst_motor()
            sleep(sleep_time)
            curr_pos = self.get_tst_position()
            delta_pos = curr_pos - start_pos
            delta_pos_diff = step - abs(delta_pos)
            diff_frac = 100 * delta_pos_diff/step
            logger.info(r'Jaws moved by: %.2f um. Deviation is %.2f um (%.2f %%)' % (delta_pos, delta_pos_diff, diff_frac))
        self.stop_tst_motor()
        self._tst_go_to_limit('close')


    def _test_communication(self):
        '''Test of all the commands sent to the stage
        '''
        logger.info("Stage name: %s" % self.stage_name)
        logger.info("Stage serial number: %s" % self.stage_sn)
        logger.info("Controller name: %s" % self.controller_name)
        logger.info("Controller serial number: %s" % self.controller_sn)
        logger.info("get_tst_position: %f" % self.get_tst_position())
        logger.info("get_tst_force: %f" % self.get_tst_force())
        logger.info("get_tst_force_limit: %f" % self.get_tst_force_limit())
        logger.info("get_tst_motor_velocity: %f" % self.get_tst_motor_velocity())
        logger.info("get_tst_motor_limit: %f" % self.get_tst_motor_limit())

        mode_list = ['velocity','force','step','cycle']
        self._test_modes(mode_list)


        logger.info("setting force limit to 10")
        self.set_tst_force_limit(10)

        logger.info("get_tst_force_limit: %f" % self.get_tst_force_limit())

        logger.info("get_tst_force: %f" % self.get_tst_force())

        logger.info("Setting jaw direction to \'open\'")
        self.set_tst_direction("open")
        logger.info('get_tst_direction: %s' % self.get_tst_direction())
        logger.info("Setting jaw direction to \'close\'")
        self.set_tst_direction("close")
        logger.info('get_tst_direction: %s' % self.get_tst_direction())

class TemperatureStage(MyStage):
    '''A class for the HFSX 350 stage. For the moment there's nothing specific to be
       defined here...
    '''

    def updateData(self):
        #logger.debug('updateData called')
        timestamp = time()
        self.curr_temp = self.get_temp()
        data = {'Timestamp': timestamp,
                'Temperature [deg C]': self.curr_temp
                }
        self.dataContainer.updateData(data)

    def test(self,dataContainer,logger_handlers, stop_test_event):
        '''Acceptance test specific for the temperature stage
        '''
        logger.handlers = logger_handlers
        self.dataContainer = dataContainer
        # Save stage details to the log file
        #logger.info("Stage name: %s" % self.stage_name)
        #logger.info("Stage serial number: %s" % self.stage_sn)
        #logger.info("Controller name: %s" % self.controller_name)
        #logger.info("Controller serial number: %s" % self.controller_sn)
        logger.debug('starting updateDeamon')
        # Start the updateDeamon thread
        stop_event = threading.Event()
        thread = threading.Thread(target=self.updateDaemon, args=(1.0,stop_event))
        thread.daemon = True
        thread.start()
        logger.info('Measuring ambient temperature for 10 s')
        sleep(10)
        rt = self.curr_temp
        logger.info('Results will be saved to %s' % self.dataContainer.outFile)
        # Set test parameters
        self.refresh_rate = 1 # [s] # How often is the temperature checked during ramps
        self.temp_delta = 0.2 # [deg C]
        stable_time = 60 # [s] # How long is the temperature kept after a ramp

        # Go to rt
        logger.info('Purging the LN tubing')
        # Ramp 1
        self.goAndWait(0, 30, stable_time, stop_test_event)
        # Ramp 2
        self.goAndWait(rt, 30, stable_time, stop_test_event)
        #self.stop_temp_ramp()
        logger.info('Starting the test protocol')
        # Ramp 3
        self.goAndWait(0, 15, stable_time, stop_test_event)
        # Ramp 4
        self.goAndWait(-100, 30, stable_time, stop_test_event)
        # Ramp 5
        self.goAndWait(-150, 15, stable_time, stop_test_event)
        # Ramp 6
        self.goAndWait(-190, 10, stable_time, stop_test_event)
        # Ramp 7
        self.goAndWait(-20, 20, stable_time, stop_test_event)
        # Ramp 8
        self.goAndWait(120, 30, stable_time, stop_test_event)
        # Ramp 9
        self.goAndWait(150, 20, stable_time, stop_test_event)
        # Ramp 10
        self.goAndWait(350, 30, stable_time, stop_test_event)
        # Ramp 11
        self.goAndWait(rt, 30, stable_time, stop_test_event)
        self.stop_temp_ramp()
        # Stop recording the temperature
        logger.info('Test protocol finished!')
        self.dataContainer.moveToFinalDir()
        stop_event.set()
        stop_test_event.set()

class HighTempStage(TemperatureStage):
    def test(self,dataContainer,logger_handlers, stop_test_event):
        '''Acceptance test specific for the high temperature stage
        '''
        logger.handlers = logger_handlers
        self.dataContainer = dataContainer
        # Save stage details to the log file
        #logger.info("Stage name: %s" % self.stage_name)
        #logger.info("Stage serial number: %s" % self.stage_sn)
        #logger.info("Controller name: %s" % self.controller_name)
        #logger.info("Controller serial number: %s" % self.controller_sn)
        logger.debug('starting updateDeamon')
        # Start the updateDeamon thread
        stop_event = threading.Event()
        thread = threading.Thread(target=self.updateDaemon, args=(1.0,stop_event))
        thread.daemon = True
        thread.start()
        logger.info('Measuring ambient temperature for 10 s')
        sleep(10)
        rt = self.curr_temp
        logger.info('Results will be saved to %s' % self.dataContainer.outFile)
        # Set test parameters
        self.refresh_rate = 1 # [s] # How often is the temperature checked during ramps
        self.temp_delta = 0.2 # [deg C]
        stable_time = 60 # [s] # How long is the temperature kept after a ramp

        logger.info('Starting the test protocol')
        # Ramp 1
        self.goAndWait(120, 30, stable_time, stop_test_event)
        # Ramp 2
        self.goAndWait(500, 100, stable_time, stop_test_event)
        # Ramp 3
        self.goAndWait(1000, 200, stable_time, stop_test_event)
        # Ramp 4
        self.goAndWait(100, 30, stable_time, stop_test_event)
        # Ramp 5
        self.goAndWait(rt, 5, stable_time, stop_test_event)
        self.stop_temp_ramp()
        # Stop recording the temperature
        logger.info('Test protocol finished!')
        self.dataContainer.moveToFinalDir()
        stop_event.set()
        stop_test_event.set()

class HumidityStage(MyStage):
    '''Defines functions specific for the humidity stage
    '''

    def get_hum(self):
        '''Return the current value of humidity in percentage
        '''
        return float(self._send_cmd('get_humidity'))

    def get_hum_target(self):
        '''Return the current value of humidity limit in percentage
        '''
        return float(self._send_cmd('get_humidity_limit'))

    def set_hum_target(self, val: float):
        '''Set the target value for relative humidity in percentage
        '''
        self._send_cmd('set_humidity_limit %f' % val)

    def start_hum_ramp(self):
        self._send_cmd('start_humidity')

    def stop_hum_ramp(self):
        self._send_cmd('stop_humidity')

    def updateData(self):
        #logger.debug('updateData called')
        self.curr_temp = self.get_temp()
        self.dataContainer.updateData(self.curr_temp)

    def test(self,dataContainer,logger_handlers, stop_test_event):
        '''Acceptance test specific for the temperature stage
        '''
        logger.handlers = logger_handlers
        self.dataContainer = dataContainer
        # Save stage details to the log file
        logger.info("Stage name: %s" % self.stage_name)
        logger.info("Stage serial number: %s" % self.stage_sn)
        logger.info("Controller name: %s" % self.controller_name)
        logger.info("Controller serial number: %s" % self.controller_sn)
        logger.debug('starting updateDeamon')
        stop_event = threading.Event()
        thread = threading.Thread(target=self.updateDaemon, args=(1.0,stop_event))
        thread.daemon = True
        thread.start()
        logger.info('Measuring ambient temperature for 10 s')
        sleep(10)
        rt = 25#self.curr_temp
        logger.info('Results will be saved to %s' % self.dataContainer.outFile)
        # Set test parameters
        self.refresh_rate = 1 # [s] # How often is the temperature checked during ramps
        self.temp_delta = 0.2 # [deg C]
        stable_time = 60 # [s] # How long is the temperature kept after a ramp

        # Go to rt
        logger.info('Purging the LN tubing')
        self.goAndWait(rt, 30, stable_time, stop_test_event)
        # Ramp 1
        self.goAndWait(0, 30, stable_time, stop_test_event)
        # Ramp 2
        self.goAndWait(rt, 30, stable_time, stop_test_event)
        #self.stop_temp_ramp()
        logger.info('Starting the test protocol')
        # Ramp 3
        self.goAndWait(0, 15, stable_time, stop_test_event)
        # Ramp 4
        self.goAndWait(-150, 30, stable_time, stop_test_event)
        # Ramp 5
        self.goAndWait(-120, 30, stable_time, stop_test_event)
        # Ramp 6
        self.goAndWait(-20, 20, stable_time, stop_test_event)
        # Ramp 7
        self.goAndWait(100, 30, stable_time, stop_test_event)
        # Ramp 8
        self.goAndWait(140, 15, stable_time, stop_test_event)
        # Ramp 9
        self.goAndWait(350, 30, stable_time, stop_test_event)
        # Ramp 10
        self.goAndWait(rt, 30, stable_time, stop_test_event)
        self.stop_temp_ramp()
        # Stop recording the temperature
        stop_event.set()
        logger.info('Test protocol finished!')
        self.dataContainer.moveToFinalDir()

class DummyStage(MyStage):
    def __init__(self):
        logger.debug('Dummy stage created')
        self.controller_name = 'Dummy'
        self.controller_sn = 'Dummy'
        self.stage_name = 'Dummy'
        self.stage_sn = 'Dummy'
        self.td = 0.1 # delay time to wait between commands
        self._observers = []

    def get_temp(self):
        return(random())

    def updateData(self):
        '''Update dummy data values'''
        timestamp = time()
        self.curr_temp = self.get_temp()
        data = {'Timestamp': timestamp,
                'Temperature [deg C]': self.curr_temp
                }
        self.dataContainer.updateData(data)

    def test(self,dataContainer,logger_handlers, stop_test_event):
        '''Dummy acceptance test result generator
        '''
        logger.handlers = logger_handlers
        self.dataContainer = dataContainer
        logger.debug('starting updateDeamon')
        # Start the updateDeamon thread
        stop_event = threading.Event()
        thread = threading.Thread(target=self.updateDaemon, args=(0.5,stop_event))
        thread.daemon = True
        thread.start()
        logger.info('Results will be saved to %s' % self.dataContainer.outFile)
        logger.info('Starting the test protocol')
        logger.info('Dummy temperature measurement no. 1')
        sleep(5)
        logger.info('Dummy temperature measurement no. 2')
        sleep(5)
        logger.error('Failed step!')
        logger.error('Testing a failed step, no reason to panic')
        logger.info('Dummy temperature measurement no. 3')
        sleep(5)
        logger.info('Test protocol finished!')
        self.dataContainer.moveToFinalDir()
        stop_event.set()
        stop_test_event.set()
        thread.join()


global known_stages

known_stages = {'TST250V': TensileStage,
                'MFS350V': TensileStage,
                'MFS350': TensileStage,
                'MFS250V': TensileStage,
                'HFSX350-CAP': TemperatureStage,
                'HFSX350-CAP-H': HumidityStage,
                'HFS600 X3M2 Xenocs stage': HighTempStage,
                'TS1000-17/3 Xenocs': HighTempStage}


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Linkam tests')
    parser.add_argument('HOST', help='Linkam server IP address')

    args = parser.parse_args()

    logger.debug('Runnign __main__ function')
    HOST = args.HOST
    PORT = 55555
    logger.debug('Trying to connect to the stage')
    stage_type = get_stage_type(HOST, 55555)
    logger.debug('Stage recognized as %s' % stage_type)
    stage = known_stages[stage_type](HOST, 55555)
    logger.debug('Testing communication')
    stage._test_communication()
    if stage_type in ["MFS350", "MFS350V"]:
        #stage._tst_test_range()
        stage._test_movement()
